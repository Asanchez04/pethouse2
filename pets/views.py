from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, redirect

from . import models
from .models import Pet
from .forms import PetForm
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from users.models import User


@permission_required('pets.add_pet')
def RegisterPet(request, id):
    """#You can register the pets in your database and show the register form."""
    user = User.objects.get(id=id)

    
    if request.method == 'POST':
        pet_form = PetForm(request.POST)
        data = []
        for value in request.POST.items():
            data.append(value[1])

        if pet_form.is_valid():
            Pet.objects.create(
                owner = user,
                name= data[1],
                age = float(data[2]),
                units = data[3],
                weight= float(data[4]),
                breed= data[5],
                species= data[6],
                gender = data[7],
                status_pet= True
            )

            return redirect(f'/pet/list/{user.id}')
    else:
        pet_form = PetForm()

    context = {
        'pet_form': pet_form,
        'owner':id

    }

    return render(request, 'pet/register_pet.html', context)


@permission_required('pets.change_pet')
def UpdatePet(request, id):
    """
    You can update the pets in your database
    """
    
    pet_form = None
    error = None
    status = None
    try:
        pet = Pet.objects.get(id=id)
        user = pet.owner.id
        if pet.status_pet:
            if request.method == 'GET':
                pet_form = PetForm(instance=pet)
            else:
                pet_form = PetForm(request.POST, instance=pet)
                if pet_form.is_valid():
                    pet_form.save()
                return redirect(f'/pet/list/{user}')
        else:
            status = "The pet is deactivated in the system"
    except ObjectDoesNotExist:
        error = "Pet Does not exist"

    context = {
        'pet_form': pet_form,
        'error': error,
        'status': status
    }
    return render(request, 'pet/update_pet.html', context)


@permission_required('pets.delete_pet')
def DeletePet(request, id):
    """
    Delete a pet changing his status account to False
    """
    
    pet = Pet.objects.get(id=id)
    user = pet.owner.id
    pet.status_pet = False
    pet.save()
    return redirect(f'/pet/list/{user}')


@permission_required('pets.view_my_pets') 
def List_pet(request, id):
    """
    You can show your pets of your database
    """
    pet = Pet.objects.filter(Q(status_pet=True) & Q(owner=id))


    context = {
        'pets': pet,
        'owner': id
    }

    return render(request, 'pet/pet_list.html', context)


@permission_required('pets.view_all_pets')
def List_all_pets(request):
    """
    Only vets can view all user's pets

    """
    pet = Pet.objects.all()

    context = {
        'pets': pet,
    }

    return render(request, 'pet/pet_all_list.html', context)
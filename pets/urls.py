from django.urls import path

from pets import views

urlpatterns = [

    path('register/<int:id>', views.RegisterPet, name='petRegister'),
    path('list/<int:id>', views.List_pet, name='petList'),
    path('update/<int:id>', views.UpdatePet, name='updatePet'),
    path('delete/<int:id>', views.DeletePet, name='deletePet'),
    path('list_all/', views.List_all_pets, name='listAllPets'),
    ]


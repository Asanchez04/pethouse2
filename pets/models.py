from django.db import models
from users.models import User


class Pet(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, unique=True)
    age = models.FloatField()
    units = models.CharField(max_length=10, default='years')
    weight = models.FloatField(blank=True,null=True)
    breed = models.CharField(max_length=50)
    species = models.CharField(max_length=30, default='dog or cat ')
    gender = models.CharField(max_length=50,default='male')
    status_pet = models.BooleanField(default=True)

    def __str__(self):
        return self.name
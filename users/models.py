from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


class User(AbstractUser):
    """Model User""" 
    cell_phone = models.CharField(max_length=50)
    address = models.CharField(max_length=255, blank=True, null=True)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    is_client = models.BooleanField(default=False)
    is_vet = models.BooleanField(default=False)
    photo = models.ImageField(upload_to='media/photos', blank=True, null=True)





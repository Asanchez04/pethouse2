import datetime

from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse
from django.shortcuts import redirect, render

from pets.models import Pet
from users.models import User
from .forms import CreateAppointmentForm
from .models import CreateAppointment
from django.core.exceptions import ObjectDoesNotExist


# Create your views here.


@permission_required('appointment.add_appointment')
def createAppointment(request, id):
    user = User.objects.get(id=id)
    pets = user.pet_set.all()

    if request.method == 'POST':
        form = CreateAppointmentForm(request.POST)
        data = []
        for value in request.POST.items():
            data.append(value[1])
        print(data)
        time = data[6]
        hora = time.split(':')
        CreateAppointment.objects.create(
            pet=Pet.objects.get(name=data[0]),
            type_service=data[2],
            date=datetime.datetime(int(data[5]), int(data[4]), int(data[3]),int(hora[0]),int(hora[1]))
        )

        return redirect(f'/appointment/list/{user.id}')
    else:
        form = CreateAppointmentForm()
    context = {
        'form': form,
        'pets': pets
    }
    return render(request, 'appointment/createAppiont.html', context)


@permission_required('appointment.view_my_appointments')
def listAppointment(request, id):
    apps = CreateAppointment.objects.all()
    data=[]
    for app in apps:
        id_client= app.pet.owner.id
        if id_client==id:
            data.append(app)

    return render(request, 'appointment/listAppoint.html', {'list': data, 'client': id})

@permission_required('appointment.change_appointment')
def updateAppointments(request, id):
    appoint = CreateAppointment.objects.get(id=id)
    client =appoint.pet.owner.id
    if request.method == 'GET':
        appointForm = CreateAppointmentForm(instance=appoint)
    else:
        appointForm = CreateAppointmentForm(request.POST, instance=appoint)
        if appointForm.is_valid():
            appointForm.save()
        return redirect(f'/appointment/list/{client}')
    context = {
        'appointForm': appointForm,
        'client': client,
    }

    return render(request, 'appointment/appointUpdate.html', context)


@permission_required('appointment.view_all_appointments')
def listAllAppointment(request):
    
    list = CreateAppointment.objects.all()

    context = {
        'list': list,
    }

    return render(request, 'appointment/list_all_appoint.html', context)

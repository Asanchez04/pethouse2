from django.db import models
from pets.models import Pet

# Create your models here.


class CreateAppointment(models.Model):
    pet = models.ForeignKey(Pet, on_delete=models.CASCADE)
    type_service = models.CharField(max_length=50)
    date = models.DateTimeField(unique=True)

    def __str__(self):
        return self.type_service + ' ' + str(self.date)



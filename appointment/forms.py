from django import forms
from .models import CreateAppointment


class CreateAppointmentForm(forms.ModelForm):
    options = (
        ('Hospitalization','Hospitalization'),
        ('Grooming','Grooming'),
        ('Treatment','Treatment'),
        ('Check Up','Check Up')
    )

    YEARS = [x for x in range(2022, 2122)]
    date = forms.DateTimeField(label='Date', widget=forms.SelectDateWidget(years=YEARS, attrs={'class': 'date'}))
    type_service = forms.CharField(label="Type Of Service", widget=forms.Select(choices=options, attrs={'class': 'form-service'}))

    class Meta:
        model = CreateAppointment
        fields = ['type_service', 'date']

